var Sequelize = require('sequelize');

function DB(dbPath, cb){
  var sequelize = new Sequelize('garden-ci','','', {
    dialect: 'sqlite',
    storage: dbPath,
    logging: false
  });

  this.jobs = sequelize.import(__dirname + "/models/job.js");

  sequelize.sync().then(function() {
    console.log("DB sync'ed. Let's go!");
    cb();
  }).catch(function(error) {
    console.log("ERROR", error);
    cb(error);
  });
}

module.exports = DB;
