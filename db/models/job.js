module.exports = function(sequelize, DataTypes) {
  return sequelize.define('job', {
    date: DataTypes.DATE,
    startDate: DataTypes.DATE,
    endDate: DataTypes.DATE,
    logs: {
      type: DataTypes.JSON,
      get: function()  {
        var value = this.getDataValue('logs');
        if(typeof value === 'string'){
          value = JSON.parse(value);
        }

        return value;
      },
    },
    params: {
      type: DataTypes.JSON,
      get: function()  {
        var value = this.getDataValue('params');
        if(typeof value === 'string'){
          value = JSON.parse(value);
        }

        return value;
      },
    },
    status: {
      type: DataTypes.STRING
    }
  }, {

  });
};
