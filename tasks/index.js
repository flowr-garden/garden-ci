module.exports = {
  clone: require('./clone'),
  build: require('./build'),
  tag: require('./tag'),
  push: require('./push'),
  cleanup: require('./cleanup'),
  checkout: require('./checkout'),
};
