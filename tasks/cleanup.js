var Promise = require("bluebird");
var command = require('./helpers/command');

function cleanup(buildData){
  return new Promise(function (resolve, reject) {
    command(
      buildData.jobId,
      'cleanup',
      'rm',
      ['-rf', buildData.project.name],
      {
        cwd: process.env.CI_TMP_DIR || '/tmp/garden-ci/'
      },
      function(err, success) {
        if(err) return reject(err);
        console.log("All done!");
        return resolve(buildData);
      }
    );
  });
}

module.exports = cleanup;
