var spawn = require('child_process').spawn;
var saveLogs = require('./saveLogs');

function command(jobId, taskName, command, args, options, cb) {
  var job = spawn(command, args || [], options || {});
  var stdout = [];
  var stderr = [];
  job.stdout.on('data', function(data) {
    stdout.push(data.toString('utf8'));
  });
  job.stderr.on('data', function(data) {
    stderr.push(data.toString('utf8'));
    console.error(data.toString('utf8'));
  });
  job.on('close', function(code) {
    saveLogs(jobId, taskName, {stdout: stdout, stderr: stderr, code: code})
    .then(function(){
      if(code === 0) {
        cb(null, true);
      } else {
        cb(code, null);
      }
    });
  });
}

module.exports = command;
