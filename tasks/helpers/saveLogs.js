var Promise = require("bluebird");

function saveLogs(id, task, logs){
  return new Promise(function (resolve, reject) {
    DB.jobs.findById(id).then(function(item){
      if(!item.logs) {
        item.logs = {};
      }
      var savedLogs = item.logs;
      savedLogs[task] = logs;
      item.logs = savedLogs;
      item.save().then(function(){
        resolve();
      });
    });
  });
}

module.exports= saveLogs;
