var Promise = require("bluebird");
var command = require('./helpers/command');

function checkout(buildData) {
  return new Promise(function (resolve, reject) {
    command(
      buildData.jobId,
      'checkout',
      'git',
      ['checkout', buildData.ref ? buildData.ref.replace('refs/tags/', '') : buildData.object_attributes.ref.replace('refs/tags/', '')],
      {
        cwd: (process.env.CI_TMP_DIR || '/tmp/garden-ci/') + buildData.project.name

      },
      function(err, success) {
        if(err) return reject(err);
        return resolve(buildData);
      }
    );
  });
}

module.exports = checkout;
