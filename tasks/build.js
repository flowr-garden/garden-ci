var Promise = require("bluebird");
var command = require('./helpers/command');

function build(buildData) {

  var args = [
    'build',
    '-t',
    ( process.env.CI_REGISTRY ? process.env.CI_REGISTRY + '/' : '') + buildData.project.path_with_namespace,
  ];

  if(buildData.object_kind === 'pipeline') {
    for (var i = 0; i < buildData.builds.length; i++) {
      if(buildData.builds[i].name === 'release') {
        args.push('--build-arg');
        args.push('GITLAB_BUILD_ID='+buildData.builds[i].id);
        break;
      }
    }
  }

  args.push('.');

  return new Promise(function (resolve, reject) {
    command(
      buildData.jobId,
      'build',
      'docker',
      args,
      {
        cwd: (process.env.CI_TMP_DIR || '/tmp/garden-ci/') + buildData.project.name
      },
      function(err, success) {
        if(err) return reject(err);
        return resolve(buildData);
      }
    );
  });
}

module.exports = build;
