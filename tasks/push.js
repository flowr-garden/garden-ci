var Promise = require("bluebird");
var command = require('./helpers/command');

function push(buildData) {
  return new Promise(function (resolve, reject) {
    command(
      buildData.jobId,
      'push',
      'docker',
      [
        'push',
        ( process.env.CI_REGISTRY ? process.env.CI_REGISTRY + '/' : '') + buildData.project.path_with_namespace
      ],
      {
        cwd: (process.env.CI_TMP_DIR || '/tmp/garden-ci/') + buildData.project.name
      },
      function(err, success) {
        if(err) return reject(err);
        return resolve(buildData);
      }
    );
  });
}

module.exports = push;
