var Promise = require("bluebird");
var command = require('./helpers/command');

function tag(buildData) {
  return new Promise(function (resolve, reject) {
    command(
      buildData.jobId,
      'tag',
      'docker',
      [
        'tag',
        ( process.env.CI_REGISTRY ? process.env.CI_REGISTRY + '/' : '') + buildData.project.path_with_namespace,
        ( process.env.CI_REGISTRY ? process.env.CI_REGISTRY + '/' : '') + buildData.project.path_with_namespace + ':' + ( buildData.ref ? buildData.ref.replace('refs/tags/', '') : buildData.object_attributes.ref )
      ],
      {
        cwd: (process.env.CI_TMP_DIR || '/tmp/garden-ci/') + buildData.project.name
      },
      function(err, success) {
        if(err) return reject(err);
        return resolve(buildData);
      }
    );
  });
}

module.exports = tag;
