var Promise = require("bluebird");
var command = require('./helpers/command');

function clone(buildData) {
  return new Promise(function (resolve, reject) {
    command(
      buildData.jobId,
      'clone',
      'git',
      ['clone', buildData.project.git_http_url],
      {
        cwd: process.env.CI_TMP_DIR || '/tmp/garden-ci/'

      },
      function(err, success) {
        if(err) return reject(err);
        return resolve(buildData);
      }
    );
  });
}

module.exports = clone;
