var pug = require('pug');

function indexAction(req, res){
  DB.jobs.findAll({
    order:'date DESC',
    limit: 50
  }).then(function(items){
    var data = {
      jobs: items
    };

    return res.render('index', data);
  });
}

function detailsAction(req, res){
  DB.jobs.findById(req.params.id)
  .then(function(item){
    var data = {
      job: item
    };

    return res.render('job', data);
  });
}

module.exports =   {
  index: indexAction,
  details: detailsAction
};
