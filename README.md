# garden-ci
Simple Docker image build and delivery tool

Only listens for **tag_push** events.

**This is a prototype/maybe-work-in-progress project.**

## Features
- Job queue (persisted to sqlite db)
- Minimal UI
- Job logs

## Requirements
- Docker
- The user running **garden-ci** should be in the *docker* group so it can execute docker commands without sudo. And it should be logged in the registry it's going to use.
- Node.js
- git

## Install
- Clone this repo: `git clone https://gitlab.com/tmpgarden/garden-ci.git`
- Enter directory and install dependencies: `cd garden-ci && npm install`

## Env vars

|name|desc|required|default|example|
|---|---|---|---|---|
| CI_TMP_DIR | directory used for jobs | no | /tmp/garden-ci | /tmp/garden-ci |
| CI_PORT | http listening port| no | 3000 | 3000 |
| **CI_SECRET** |webhook secret| **yes**| --- | so-secret |
| CI_REGISTRY |private docker registry| no | --- | registry.gitlab.com |


## Run
- Execute `npm start`

## Webhooks

### Gitlab

Add a webhook for each project you want to build. The endpoint should be something like this:

`http://<your-garden-ci-host>:<garden-ci-port>/webhooks/gitlab`

You must provide the secret used when running the garden-ci instance.

## Screenshots
![Imgur](http://i.imgur.com/yxTrnqO.png)
![Imgur](http://i.imgur.com/kJdRSjP.png)
