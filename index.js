var fs = require('fs-extra');
var express = require('express');
var bodyParser = require('body-parser');
var Database = require('./db');

var ui = require('./ui');
var Tasks = require('./tasks');

fs.ensureDirSync(process.env.CI_TMP_DIR || '/tmp/garden-ci/');
fs.emptydirSync(process.env.CI_TMP_DIR || '/tmp/garden-ci/');

var isWorking = false;
var queue = [];

GLOBAL.DB = new Database( __dirname + '/garden-ci.sqlite', function(err){
  if(err) {
    console.log(err);
  } else {
    DB.jobs.findAll({
      where: {
        status: 'running'
      }
    }).then(function(runningJobs){
      for (var i = 0; i < runningJobs.length; i++) {
        var item = runningJobs[i].dataValues;
        item.params = JSON.parse(item.params);
        queue.push(item);
      }
      DB.jobs.findAll({
        where: {
          status: 'queued'
        }
      }).then(function(items){
        for (var i = 0; i < items.length; i++) {
          var item = items[i].dataValues;
          item.params = JSON.parse(item.params);
          queue.push(item);
        }

        processQueue();
      });
    });
  }
});

var app = express();
app.locals.moment = require('moment');
app.set('views', './ui/views');
app.set('view engine', 'pug');
app.use(bodyParser.json());

app.get('/', ui.index);
app.get('/build/:id', ui.details);

app.get('/css/bulma.css', function(req, res){
  var options = {
    root: __dirname + '/node_modules/bulma/css/'
  };
  return res.sendFile('bulma.css', options);
});

app.post('/webhooks/gitlab', function (req, res) {
  var secret = req.headers['x-gitlab-token'];
  if( process.env.CI_SECRET === secret) {
    if(req.body.object_kind == 'tag_push') {
      DB.jobs
      .create({ date: new Date(), params: req.body, status: 'queued' })
      .then(function(item){
        addToQueue(item.dataValues);
        processQueue();
      });
    } else if(req.body.object_kind == 'pipeline' && req.body.object_attributes.status === 'success') {
      DB.jobs
      .create({ date: new Date(), params: req.body, status: 'queued' })
      .then(function(item){
        addToQueue(item.dataValues);
        processQueue();
      });
    }

    return res.sendStatus(200);
  } else {
    console.error("Secrets dont match!");
    return res.sendStatus(401);
  }
});

app.listen(process.env.CI_PORT || 3000, function () {
  console.log('Example app listening on port ' + (process.env.CI_PORT || 3000) );
});

function addToQueue(item){
  console.log("queued");
  queue.push(item);
}

function processQueue(){
  if(isWorking) return console.log("is working");

  if(queue.length) {
    isWorking = true;
    console.log("lets do our job");
    var job = queue[0];
    DB.jobs.findById(job.id).then(function(item){
      item.update({status: 'running', startDate: new Date()});
    });
    job.params.jobId = job.id; // ¬¬
    return Tasks.clone(job.params)
    .then(Tasks.checkout)
    .then(Tasks.build)
    .then(Tasks.tag)
    .then(Tasks.push)
    .then(Tasks.cleanup)
    .then(function(){
      DB.jobs.findById(job.id).then(function(item){
        item.update({status: 'done', endDate: new Date()});
        queue.shift();
        isWorking = false;
        processQueue();
      });
    })
    .catch(function(){
      Tasks.cleanup(job.params);
      DB.jobs.findById(job.id).then(function(item){
        item.update({status: 'failed', endDate: new Date()});
        queue.shift();
        isWorking = false;
        processQueue();
      });
    });
  } else {
    return console.log("no more jobs!");
  }
}
